package nlp

object EntityServiceGrpc {
  val METHOD_GET_ENTITIES: _root_.io.grpc.MethodDescriptor[nlp.Request, nlp.EntityResponse] =
    _root_.io.grpc.MethodDescriptor.newBuilder()
      .setType(_root_.io.grpc.MethodDescriptor.MethodType.UNARY)
      .setFullMethodName(_root_.io.grpc.MethodDescriptor.generateFullMethodName("EntityService", "GetEntities"))
      .setSampledToLocalTracing(true)
      .setRequestMarshaller(_root_.scalapb.grpc.Marshaller.forMessage[nlp.Request])
      .setResponseMarshaller(_root_.scalapb.grpc.Marshaller.forMessage[nlp.EntityResponse])
      .setSchemaDescriptor(_root_.scalapb.grpc.ConcreteProtoMethodDescriptorSupplier.fromMethodDescriptor(nlp.NlpProto.javaDescriptor.getServices.get(1).getMethods.get(0)))
      .build()
  
  val METHOD_STREAM_ENTITIES: _root_.io.grpc.MethodDescriptor[nlp.Request, nlp.EntityResponse] =
    _root_.io.grpc.MethodDescriptor.newBuilder()
      .setType(_root_.io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
      .setFullMethodName(_root_.io.grpc.MethodDescriptor.generateFullMethodName("EntityService", "StreamEntities"))
      .setSampledToLocalTracing(true)
      .setRequestMarshaller(_root_.scalapb.grpc.Marshaller.forMessage[nlp.Request])
      .setResponseMarshaller(_root_.scalapb.grpc.Marshaller.forMessage[nlp.EntityResponse])
      .setSchemaDescriptor(_root_.scalapb.grpc.ConcreteProtoMethodDescriptorSupplier.fromMethodDescriptor(nlp.NlpProto.javaDescriptor.getServices.get(1).getMethods.get(1)))
      .build()
  
  val METHOD_BIDI_ENTITIES: _root_.io.grpc.MethodDescriptor[nlp.Request, nlp.EntityResponse] =
    _root_.io.grpc.MethodDescriptor.newBuilder()
      .setType(_root_.io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
      .setFullMethodName(_root_.io.grpc.MethodDescriptor.generateFullMethodName("EntityService", "BidiEntities"))
      .setSampledToLocalTracing(true)
      .setRequestMarshaller(_root_.scalapb.grpc.Marshaller.forMessage[nlp.Request])
      .setResponseMarshaller(_root_.scalapb.grpc.Marshaller.forMessage[nlp.EntityResponse])
      .setSchemaDescriptor(_root_.scalapb.grpc.ConcreteProtoMethodDescriptorSupplier.fromMethodDescriptor(nlp.NlpProto.javaDescriptor.getServices.get(1).getMethods.get(2)))
      .build()
  
  val SERVICE: _root_.io.grpc.ServiceDescriptor =
    _root_.io.grpc.ServiceDescriptor.newBuilder("EntityService")
      .setSchemaDescriptor(new _root_.scalapb.grpc.ConcreteProtoFileDescriptorSupplier(nlp.NlpProto.javaDescriptor))
      .addMethod(METHOD_GET_ENTITIES)
      .addMethod(METHOD_STREAM_ENTITIES)
      .addMethod(METHOD_BIDI_ENTITIES)
      .build()
  
  trait EntityService extends _root_.scalapb.grpc.AbstractService {
    override def serviceCompanion = EntityService
    def getEntities(request: nlp.Request): scala.concurrent.Future[nlp.EntityResponse]
    def streamEntities(request: nlp.Request, responseObserver: _root_.io.grpc.stub.StreamObserver[nlp.EntityResponse]): Unit
    def bidiEntities(responseObserver: _root_.io.grpc.stub.StreamObserver[nlp.EntityResponse]): _root_.io.grpc.stub.StreamObserver[nlp.Request]
  }
  
  object EntityService extends _root_.scalapb.grpc.ServiceCompanion[EntityService] {
    implicit def serviceCompanion: _root_.scalapb.grpc.ServiceCompanion[EntityService] = this
    def javaDescriptor: _root_.com.google.protobuf.Descriptors.ServiceDescriptor = nlp.NlpProto.javaDescriptor.getServices.get(1)
    def scalaDescriptor: _root_.scalapb.descriptors.ServiceDescriptor = nlp.NlpProto.scalaDescriptor.services(1)
  }
  
  trait EntityServiceBlockingClient {
    def serviceCompanion = EntityService
    def getEntities(request: nlp.Request): nlp.EntityResponse
    def streamEntities(request: nlp.Request): scala.collection.Iterator[nlp.EntityResponse]
  }
  
  class EntityServiceBlockingStub(channel: _root_.io.grpc.Channel, options: _root_.io.grpc.CallOptions = _root_.io.grpc.CallOptions.DEFAULT) extends _root_.io.grpc.stub.AbstractStub[EntityServiceBlockingStub](channel, options) with EntityServiceBlockingClient {
    override def getEntities(request: nlp.Request): nlp.EntityResponse = {
      _root_.scalapb.grpc.ClientCalls.blockingUnaryCall(channel, METHOD_GET_ENTITIES, options, request)
    }
    
    override def streamEntities(request: nlp.Request): scala.collection.Iterator[nlp.EntityResponse] = {
      _root_.scalapb.grpc.ClientCalls.blockingServerStreamingCall(channel, METHOD_STREAM_ENTITIES, options, request)
    }
    
    override def build(channel: _root_.io.grpc.Channel, options: _root_.io.grpc.CallOptions): EntityServiceBlockingStub = new EntityServiceBlockingStub(channel, options)
  }
  
  class EntityServiceStub(channel: _root_.io.grpc.Channel, options: _root_.io.grpc.CallOptions = _root_.io.grpc.CallOptions.DEFAULT) extends _root_.io.grpc.stub.AbstractStub[EntityServiceStub](channel, options) with EntityService {
    override def getEntities(request: nlp.Request): scala.concurrent.Future[nlp.EntityResponse] = {
      _root_.scalapb.grpc.ClientCalls.asyncUnaryCall(channel, METHOD_GET_ENTITIES, options, request)
    }
    
    override def streamEntities(request: nlp.Request, responseObserver: _root_.io.grpc.stub.StreamObserver[nlp.EntityResponse]): Unit = {
      _root_.scalapb.grpc.ClientCalls.asyncServerStreamingCall(channel, METHOD_STREAM_ENTITIES, options, request, responseObserver)
    }
    
    override def bidiEntities(responseObserver: _root_.io.grpc.stub.StreamObserver[nlp.EntityResponse]): _root_.io.grpc.stub.StreamObserver[nlp.Request] = {
      _root_.scalapb.grpc.ClientCalls.asyncBidiStreamingCall(channel, METHOD_BIDI_ENTITIES, options, responseObserver)
    }
    
    override def build(channel: _root_.io.grpc.Channel, options: _root_.io.grpc.CallOptions): EntityServiceStub = new EntityServiceStub(channel, options)
  }
  
  def bindService(serviceImpl: EntityService, executionContext: scala.concurrent.ExecutionContext): _root_.io.grpc.ServerServiceDefinition =
    _root_.io.grpc.ServerServiceDefinition.builder(SERVICE)
    .addMethod(
      METHOD_GET_ENTITIES,
      _root_.io.grpc.stub.ServerCalls.asyncUnaryCall(new _root_.io.grpc.stub.ServerCalls.UnaryMethod[nlp.Request, nlp.EntityResponse] {
        override def invoke(request: nlp.Request, observer: _root_.io.grpc.stub.StreamObserver[nlp.EntityResponse]): Unit =
          serviceImpl.getEntities(request).onComplete(scalapb.grpc.Grpc.completeObserver(observer))(
            executionContext)
      }))
    .addMethod(
      METHOD_STREAM_ENTITIES,
      _root_.io.grpc.stub.ServerCalls.asyncServerStreamingCall(new _root_.io.grpc.stub.ServerCalls.ServerStreamingMethod[nlp.Request, nlp.EntityResponse] {
        override def invoke(request: nlp.Request, observer: _root_.io.grpc.stub.StreamObserver[nlp.EntityResponse]): Unit =
          serviceImpl.streamEntities(request, observer)
      }))
    .addMethod(
      METHOD_BIDI_ENTITIES,
      _root_.io.grpc.stub.ServerCalls.asyncBidiStreamingCall(new _root_.io.grpc.stub.ServerCalls.BidiStreamingMethod[nlp.Request, nlp.EntityResponse] {
        override def invoke(observer: _root_.io.grpc.stub.StreamObserver[nlp.EntityResponse]): _root_.io.grpc.stub.StreamObserver[nlp.Request] =
          serviceImpl.bidiEntities(observer)
      }))
    .build()
  
  def blockingStub(channel: _root_.io.grpc.Channel): EntityServiceBlockingStub = new EntityServiceBlockingStub(channel)
  
  def stub(channel: _root_.io.grpc.Channel): EntityServiceStub = new EntityServiceStub(channel)
  
  def javaDescriptor: _root_.com.google.protobuf.Descriptors.ServiceDescriptor = nlp.NlpProto.javaDescriptor.getServices.get(1)
  
}