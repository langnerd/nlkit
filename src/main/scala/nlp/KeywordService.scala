package nlp


package object keywordService {
  type KeywordService = zio.Has[KeywordService.Service[Any]]
  
  object KeywordService {
    trait Service[R] { self =>
      def getKeywords(request: nlp.Request): _root_.zio.ZIO[R, io.grpc.Status, nlp.KeywordResponse]
      def provide_(env: R): Service[Any] = new Service[Any] {
        def getKeywords(request: nlp.Request): _root_.zio.ZIO[Any, io.grpc.Status, nlp.KeywordResponse] = self.getKeywords(request).provide(env)
      }
    }
    
    def client(channel: io.grpc.Channel, options: io.grpc.CallOptions = io.grpc.CallOptions.DEFAULT, headers: => io.grpc.Metadata = new io.grpc.Metadata()): KeywordService.Service[Any] = new KeywordService.Service[Any] {
      def getKeywords(request: nlp.Request): _root_.zio.ZIO[Any, io.grpc.Status, nlp.KeywordResponse] = scalapb.zio_grpc.client.ClientCalls.unaryCall(
        scalapb.zio_grpc.client.ZClientCall(channel.newCall(nlp.KeywordServiceGrpc.METHOD_GET_KEYWORDS, options)),
        headers,
        request
      )
    }
    
    def clientService(channel: io.grpc.Channel, options: io.grpc.CallOptions = io.grpc.CallOptions.DEFAULT, headers: => io.grpc.Metadata = new io.grpc.Metadata()): KeywordService = _root_.zio.Has(
      client(channel, options, headers)
    )
    
    def getKeywords(request: nlp.Request): _root_.zio.ZIO[KeywordService, io.grpc.Status, nlp.KeywordResponse] = _root_.zio.ZIO.accessM(_.get.getKeywords(request))
    
    def bindService[R](runtime: zio.Runtime[R], serviceImpl: KeywordService.Service[R]): _root_.io.grpc.ServerServiceDefinition =
      _root_.io.grpc.ServerServiceDefinition.builder(nlp.KeywordServiceGrpc.SERVICE)
      .addMethod(
        nlp.KeywordServiceGrpc.METHOD_GET_KEYWORDS,
        _root_.scalapb.zio_grpc.server.ZServerCallHandler.unaryCallHandler(runtime, serviceImpl.getKeywords)
      )
      .build()
  }
}