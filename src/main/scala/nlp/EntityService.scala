package nlp


package object entityService {
  type EntityService = zio.Has[EntityService.Service[Any]]
  
  object EntityService {
    trait Service[R] { self =>
      def getEntities(request: nlp.Request): _root_.zio.ZIO[R, io.grpc.Status, nlp.EntityResponse]
      def streamEntities(request: nlp.Request): _root_.zio.stream.ZStream[R, io.grpc.Status, nlp.EntityResponse]
      def bidiEntities(request: _root_.zio.stream.ZStream[Any, io.grpc.Status, nlp.Request]): _root_.zio.stream.ZStream[R, io.grpc.Status, nlp.EntityResponse]
      def provide_(env: R): Service[Any] = new Service[Any] {
        def getEntities(request: nlp.Request): _root_.zio.ZIO[Any, io.grpc.Status, nlp.EntityResponse] = self.getEntities(request).provide(env)
        def streamEntities(request: nlp.Request): _root_.zio.stream.ZStream[Any, io.grpc.Status, nlp.EntityResponse] = self.streamEntities(request).provide(env)
        def bidiEntities(request: _root_.zio.stream.ZStream[Any, io.grpc.Status, nlp.Request]): _root_.zio.stream.ZStream[Any, io.grpc.Status, nlp.EntityResponse] = self.bidiEntities(request).provide(env)
      }
    }
    
    def client(channel: io.grpc.Channel, options: io.grpc.CallOptions = io.grpc.CallOptions.DEFAULT, headers: => io.grpc.Metadata = new io.grpc.Metadata()): EntityService.Service[Any] = new EntityService.Service[Any] {
      def getEntities(request: nlp.Request): _root_.zio.ZIO[Any, io.grpc.Status, nlp.EntityResponse] = scalapb.zio_grpc.client.ClientCalls.unaryCall(
        scalapb.zio_grpc.client.ZClientCall(channel.newCall(nlp.EntityServiceGrpc.METHOD_GET_ENTITIES, options)),
        headers,
        request
      )
      def streamEntities(request: nlp.Request): _root_.zio.stream.ZStream[Any, io.grpc.Status, nlp.EntityResponse] = scalapb.zio_grpc.client.ClientCalls.serverStreamingCall(
        scalapb.zio_grpc.client.ZClientCall(channel.newCall(nlp.EntityServiceGrpc.METHOD_STREAM_ENTITIES, options)),
        headers,
        request
      )
      def bidiEntities(request: _root_.zio.stream.ZStream[Any, io.grpc.Status, nlp.Request]): _root_.zio.stream.ZStream[Any, io.grpc.Status, nlp.EntityResponse] = scalapb.zio_grpc.client.ClientCalls.bidiCall(
        scalapb.zio_grpc.client.ZClientCall(channel.newCall(nlp.EntityServiceGrpc.METHOD_BIDI_ENTITIES, options)),
        headers,
        request
      )
    }
    
    def clientService(channel: io.grpc.Channel, options: io.grpc.CallOptions = io.grpc.CallOptions.DEFAULT, headers: => io.grpc.Metadata = new io.grpc.Metadata()): EntityService = _root_.zio.Has(
      client(channel, options, headers)
    )
    
    def getEntities(request: nlp.Request): _root_.zio.ZIO[EntityService, io.grpc.Status, nlp.EntityResponse] = _root_.zio.ZIO.accessM(_.get.getEntities(request))
    def streamEntities(request: nlp.Request): _root_.zio.stream.ZStream[EntityService, io.grpc.Status, nlp.EntityResponse] = _root_.zio.stream.ZStream.accessStream(_.get.streamEntities(request))
    def bidiEntities(request: _root_.zio.stream.ZStream[Any, io.grpc.Status, nlp.Request]): _root_.zio.stream.ZStream[EntityService, io.grpc.Status, nlp.EntityResponse] = _root_.zio.stream.ZStream.accessStream(_.get.bidiEntities(request))
    
    def bindService[R](runtime: zio.Runtime[R], serviceImpl: EntityService.Service[R]): _root_.io.grpc.ServerServiceDefinition =
      _root_.io.grpc.ServerServiceDefinition.builder(nlp.EntityServiceGrpc.SERVICE)
      .addMethod(
        nlp.EntityServiceGrpc.METHOD_GET_ENTITIES,
        _root_.scalapb.zio_grpc.server.ZServerCallHandler.unaryCallHandler(runtime, serviceImpl.getEntities)
      )
      .addMethod(
        nlp.EntityServiceGrpc.METHOD_STREAM_ENTITIES,
        _root_.scalapb.zio_grpc.server.ZServerCallHandler.serverStreamingCallHandler(runtime, serviceImpl.streamEntities)
      )
      .addMethod(
        nlp.EntityServiceGrpc.METHOD_BIDI_ENTITIES,
        _root_.scalapb.zio_grpc.server.ZServerCallHandler.bidiCallHandler(runtime, serviceImpl.bidiEntities)
      )
      .build()
  }
}