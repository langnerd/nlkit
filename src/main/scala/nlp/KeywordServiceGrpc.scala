package nlp

object KeywordServiceGrpc {
  val METHOD_GET_KEYWORDS: _root_.io.grpc.MethodDescriptor[nlp.Request, nlp.KeywordResponse] =
    _root_.io.grpc.MethodDescriptor.newBuilder()
      .setType(_root_.io.grpc.MethodDescriptor.MethodType.UNARY)
      .setFullMethodName(_root_.io.grpc.MethodDescriptor.generateFullMethodName("KeywordService", "GetKeywords"))
      .setSampledToLocalTracing(true)
      .setRequestMarshaller(_root_.scalapb.grpc.Marshaller.forMessage[nlp.Request])
      .setResponseMarshaller(_root_.scalapb.grpc.Marshaller.forMessage[nlp.KeywordResponse])
      .setSchemaDescriptor(_root_.scalapb.grpc.ConcreteProtoMethodDescriptorSupplier.fromMethodDescriptor(nlp.NlpProto.javaDescriptor.getServices.get(0).getMethods.get(0)))
      .build()
  
  val SERVICE: _root_.io.grpc.ServiceDescriptor =
    _root_.io.grpc.ServiceDescriptor.newBuilder("KeywordService")
      .setSchemaDescriptor(new _root_.scalapb.grpc.ConcreteProtoFileDescriptorSupplier(nlp.NlpProto.javaDescriptor))
      .addMethod(METHOD_GET_KEYWORDS)
      .build()
  
  trait KeywordService extends _root_.scalapb.grpc.AbstractService {
    override def serviceCompanion = KeywordService
    def getKeywords(request: nlp.Request): scala.concurrent.Future[nlp.KeywordResponse]
  }
  
  object KeywordService extends _root_.scalapb.grpc.ServiceCompanion[KeywordService] {
    implicit def serviceCompanion: _root_.scalapb.grpc.ServiceCompanion[KeywordService] = this
    def javaDescriptor: _root_.com.google.protobuf.Descriptors.ServiceDescriptor = nlp.NlpProto.javaDescriptor.getServices.get(0)
    def scalaDescriptor: _root_.scalapb.descriptors.ServiceDescriptor = nlp.NlpProto.scalaDescriptor.services(0)
  }
  
  trait KeywordServiceBlockingClient {
    def serviceCompanion = KeywordService
    def getKeywords(request: nlp.Request): nlp.KeywordResponse
  }
  
  class KeywordServiceBlockingStub(channel: _root_.io.grpc.Channel, options: _root_.io.grpc.CallOptions = _root_.io.grpc.CallOptions.DEFAULT) extends _root_.io.grpc.stub.AbstractStub[KeywordServiceBlockingStub](channel, options) with KeywordServiceBlockingClient {
    override def getKeywords(request: nlp.Request): nlp.KeywordResponse = {
      _root_.scalapb.grpc.ClientCalls.blockingUnaryCall(channel, METHOD_GET_KEYWORDS, options, request)
    }
    
    override def build(channel: _root_.io.grpc.Channel, options: _root_.io.grpc.CallOptions): KeywordServiceBlockingStub = new KeywordServiceBlockingStub(channel, options)
  }
  
  class KeywordServiceStub(channel: _root_.io.grpc.Channel, options: _root_.io.grpc.CallOptions = _root_.io.grpc.CallOptions.DEFAULT) extends _root_.io.grpc.stub.AbstractStub[KeywordServiceStub](channel, options) with KeywordService {
    override def getKeywords(request: nlp.Request): scala.concurrent.Future[nlp.KeywordResponse] = {
      _root_.scalapb.grpc.ClientCalls.asyncUnaryCall(channel, METHOD_GET_KEYWORDS, options, request)
    }
    
    override def build(channel: _root_.io.grpc.Channel, options: _root_.io.grpc.CallOptions): KeywordServiceStub = new KeywordServiceStub(channel, options)
  }
  
  def bindService(serviceImpl: KeywordService, executionContext: scala.concurrent.ExecutionContext): _root_.io.grpc.ServerServiceDefinition =
    _root_.io.grpc.ServerServiceDefinition.builder(SERVICE)
    .addMethod(
      METHOD_GET_KEYWORDS,
      _root_.io.grpc.stub.ServerCalls.asyncUnaryCall(new _root_.io.grpc.stub.ServerCalls.UnaryMethod[nlp.Request, nlp.KeywordResponse] {
        override def invoke(request: nlp.Request, observer: _root_.io.grpc.stub.StreamObserver[nlp.KeywordResponse]): Unit =
          serviceImpl.getKeywords(request).onComplete(scalapb.grpc.Grpc.completeObserver(observer))(
            executionContext)
      }))
    .build()
  
  def blockingStub(channel: _root_.io.grpc.Channel): KeywordServiceBlockingStub = new KeywordServiceBlockingStub(channel)
  
  def stub(channel: _root_.io.grpc.Channel): KeywordServiceStub = new KeywordServiceStub(channel)
  
  def javaDescriptor: _root_.com.google.protobuf.Descriptors.ServiceDescriptor = nlp.NlpProto.javaDescriptor.getServices.get(0)
  
}