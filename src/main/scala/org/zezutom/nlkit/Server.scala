package org.zezutom.nlkit

import io.grpc.ServerBuilder
import nlp.entityService.EntityService
import org.zezutom.nlkit.service.EntityRecognitionService
import zio.ZIO
import zio.clock.Clock
import zio.console.Console
import zio.blocking._
import zio.console._
import zio.duration._

object Server extends zio.App {

  override def run(args: List[String]): ZIO[zio.ZEnv, Nothing, Int] =
    myAppLogic.fold(_ => 1, _ => 0)

  private val myAppLogic = runServer(8000)

  private def serverWait =
    for {
      _ <- putStrLn("Server is running. Press Ctrl-C to stop")
      _ <- (putStr(".") *> ZIO.sleep(1.second)).forever
    } yield ()

  def runServer(
      port: Int): ZIO[Clock with Blocking with Console, Throwable, Unit] = {
    for {
      rts <- ZIO.runtime[Clock with Console]
      builder = ServerBuilder
        .forPort(port)
        .addService(
          EntityService.bindService(rts, EntityRecognitionService.Live))
      server = scalapb.zio_grpc.Server.managed(builder).useForever
      _ <- server raceAttempt serverWait
    } yield ()
  }
}
