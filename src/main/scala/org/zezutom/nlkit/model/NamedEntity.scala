package org.zezutom.nlkit.model

case class NamedEntity(label: String, text: String)
