package org.zezutom.nlkit.service

import io.grpc.{ManagedChannelBuilder, Status}
import nlp.entityService.EntityService
import nlp.{EntityResponse, Request}
import scalapb.zio_grpc.ZManagedChannel
import zio.clock.Clock
import zio.stream.{Stream, ZStream}
import zio.{Schedule, ZIO, ZLayer}
import zio.duration._

object EntityRecognitionService {
  object Live extends EntityService.Service[Clock] {
    override def getEntities(
        request: Request): ZIO[Any, Status, EntityResponse] = {
      for {
        response <- EntityService
          .getEntities(request)
          .provideLayer(env)
          .mapError(_ => Status.INTERNAL)
      } yield response
    }

    override def streamEntities(
        request: Request): ZStream[Clock, Status, EntityResponse] = {

      val effects = for {
        resp <- request.text.split("\\.") map { chunk =>
          EntityService
            .getEntities(Request(chunk))
            .provideLayer(env)
            .mapError(_ => Status.INTERNAL)
        }
      } yield resp

      Stream
        .fromIterable(effects)
        .mapM(identity)
        .scheduleElements(Schedule.spaced(1000.millis))
        .forever
        .take(5) ++
        Stream.fail(
          Status.INTERNAL
            .withDescription("There was an error!")
            .withCause(new RuntimeException))
    }

    override def bidiEntities(request: ZStream[Any, Status, Request])
      : ZStream[Clock, Status, EntityResponse] = {
      request.mapM { req =>
        EntityService
          .getEntities(req)
          .provideLayer(env)
          .mapError(_ => Status.INTERNAL)
      } ++
        Stream.fail(
          Status.INTERNAL
            .withDescription("There was an error!")
            .withCause(new RuntimeException))
    }

    val env = ZLayer.fromManaged {
      val builder =
        ManagedChannelBuilder.forAddress("localhost", 6000).usePlaintext()
      ZManagedChannel.make(builder).map(EntityService.clientService(_))
    }

  }
}
