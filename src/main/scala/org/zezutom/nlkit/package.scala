package org.zezutom

import io.grpc.ManagedChannelBuilder
import nlp.entityService.EntityService
import scalapb.zio_grpc.ZManagedChannel
import zio.ZLayer

package object nlkit {

  val env = ZLayer.fromManaged {
    val builder =
      ManagedChannelBuilder.forAddress("localhost", 8000).usePlaintext()
    ZManagedChannel.make(builder).map(EntityService.clientService(_))
  }

  val exampleText =
    """
    Babylon was a significant city in ancient Mesopotamia,
    in the fertile plain between the Tigris and Euphrates rivers.
    The city was built upon the Euphrates, and divided in equal parts along its left and right banks,
    with steep embankments to contain the river's seasonal floods.
    Hammurabi was the sixth Amorite king of Babylon
    from 1792 BC to 1750 BC middle chronology.
    He became the first king of the Babylonian Empire following the abdication of his father,
    Sin-Muballit, who had become very ill and died, extending Babylon's control over Mesopotamia
    by winning a series of wars against neighboring kingdoms.
    """

  val exampleTexts = List(
    """
    Babylon was a significant city in ancient Mesopotamia,
    in the fertile plain between the Tigris and Euphrates rivers.
    The city was built upon the Euphrates, and divided in equal parts along its left and right banks,
    """,
    """
    with steep embankments to contain the river's seasonal floods.
    Hammurabi was the sixth Amorite king of Babylon
    from 1792 BC to 1750 BC middle chronology.
    """,
    """
    He became the first king of the Babylonian Empire following the abdication of his father,
    Sin-Muballit, who had become very ill and died, extending Babylon's control over Mesopotamia
    by winning a series of wars against neighboring kingdoms.
    """
  )
}
