package org.zezutom.nlkit.client

import nlp.entityService.EntityService
import nlp.{Request, NamedEntity => ApiNamedEntity}
import org.zezutom.nlkit._
import org.zezutom.nlkit.model.NamedEntity
import zio.ZIO
import zio.console.{Console, putStrLn}

/**
 * Unary RPC request
 * See https://grpc.io/docs/guides/concepts/
 */
object UnaryClient extends zio.App {

  override def run(args: List[String]): ZIO[zio.ZEnv, Nothing, Int] =
    myAppLogic.fold({ _ => 1}, _ => 0)

  private def parseModel(api: ApiNamedEntity): NamedEntity =
    NamedEntity(api.label, api.text)

  private def myAppLogic =
    (for {
      r <- EntityService.getEntities(Request(exampleText))
      entities = r.entities.map(parseModel).distinct.sortBy(x => (x.label, x.text))
      _ <- putStrLn(entities.mkString("\n"))
    } yield ()).provideLayer(env ++ Console.live)
}
