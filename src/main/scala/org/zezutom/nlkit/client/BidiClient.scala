package org.zezutom.nlkit.client

import nlp.entityService.EntityService
import nlp.{Request, NamedEntity => ApiNamedEntity}
import org.zezutom.nlkit._
import org.zezutom.nlkit.model.NamedEntity
import zio.ZIO
import zio.console.{Console, putStrLn}
import zio.stream.Stream

object BidiClient extends zio.App {

  override def run(args: List[String]): ZIO[zio.ZEnv, Nothing, Int] =
    myAppLogic.fold({ _ => 1}, _ => 0)

  private def parseModel(api: ApiNamedEntity): NamedEntity =
    NamedEntity(api.label, api.text)

  private def myAppLogic = {
    val f = exampleTexts.map(t => ZIO.succeed(Request(t)))
    val reqs = Stream.fromIterable(f).mapM(identity)

    (for {
      _ <- EntityService
        .bidiEntities(reqs)
        .foreach(r => putStrLn(r.entities.map(parseModel).mkString("\n")))
    } yield ()).provideLayer(env ++ Console.live)
  }
}
