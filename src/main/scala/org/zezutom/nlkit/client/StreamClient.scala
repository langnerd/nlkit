package org.zezutom.nlkit.client

import nlp.entityService.EntityService
import nlp.{Request, NamedEntity => ApiNamedEntity}
import org.zezutom.nlkit._
import org.zezutom.nlkit.model.NamedEntity
import zio.ZIO
import zio.console.{Console, putStrLn}

object StreamClient extends zio.App {

  override def run(args: List[String]): ZIO[zio.ZEnv, Nothing, Int] =
    myAppLogic.fold({ _ => 1}, _ => 0)

  private def parseModel(api: ApiNamedEntity): NamedEntity =
    NamedEntity(api.label, api.text)

  private def myAppLogic =
    (for {
      _ <- EntityService
        .streamEntities(Request(exampleText))
        .foreach(r => putStrLn(r.entities.map(parseModel).mkString("\n")))
    } yield ()).provideLayer(env ++ Console.live)
}
