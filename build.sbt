name := "nlkit_pipeline"

version := "0.1"

scalaVersion := "2.13.1"


lazy val root = project
  .in(file("."))
  .enablePlugins(sbtprotoc.ProtocPlugin)
  .settings(commonSettings)

lazy val compilerOptions = Seq(
  "-unchecked",
  "-feature",
  "-language:existentials",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-language:postfixOps",
  "-deprecation",
  "-encoding",
  "utf8"
)

lazy val commonSettings = Seq(
  scalacOptions ++= compilerOptions,
  resolvers ++= Seq(Resolver.sonatypeRepo("releases"))
) //++ Seq(scalafmtOnCompile := true)

val catsEffectVersion = "2.1.1"
val scalaTestVersion = "3.0.8"

val grpcVersion = "1.26.0"

resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

PB.targets in Compile := Seq(
  scalapb.gen(grpc = true) -> (sourceManaged in Compile).value,
  scalapb.zio_grpc.ZioCodeGenerator -> (sourceManaged in Compile).value
)

libraryDependencies ++= Seq(
  "com.thesamet.scalapb" %% "scalapb-runtime-grpc" % scalapb.compiler.Version.scalapbVersion,
  "io.grpc" % "grpc-netty" % grpcVersion
)

