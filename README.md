# NLKit
Scala toolkit for Natural Language Processing.

## Features
* gRPC server and client based on [zio-grpc](https://github.com/scalapb/zio-grpc)
* integration with Python NLP pipeline, see [NLKit gRPC Server](https://gitlab.com/langnerd/nlkit-grpc-server)
* functional and scalable

## Usage

Prerequisite: Checkout [NLKit gRPC Server](https://gitlab.com/langnerd/nlkit-grpc-server) and run the Python gRPC server as per instructions in the repo.

* Run the [Server](src/main/scala/org/zezutom/nlkit/Server.scala)
* in a separate terminal tab run any of the following test clients:
  * [UnaryClient](src/main/scala/org/zezutom/nlkit/client/UnaryClient.scala) to test unary RPC
  * [StreamClient](src/main/scala/org/zezutom/nlkit/client/StreamClient.scala) to play with streaming
  * [BidiClient](src/main/scala/org/zezutom/nlkit/client/BidiClient.scala) to check bidirectional communication
  
In terms of NLP, only Entity Recognition through NLTK is supported. More features to follow.
